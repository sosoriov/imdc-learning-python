### The wait is over!
#### Finally we will start our introductory **Python** course on 27/04/2018

### What you will learn?

The course is divided in several units starting from the basics (see below). Each unit contains examples an learning material which you will master (I hope :) )

### What do you need?

You need your laptop and you should install **Python** and **PyCharm**

#### Python Installation:

* Option 1: Install Anaconda. **RECOMMENDED**!
Anaconda is a free Python distribution that includes many commonly used Python packages.

***Steps***
1.   Go to the [Anaconda download page](https://www.anaconda.com/download/).
2.   Choose your operating system.
3.   Under "Python 3.6" click on "Windows 64-bit Graphical Installer" or "Windows 32-bit Graphical Installer", depending on what operating system you have.
4.   Download Python.
5.   Click on the .exe file you just downloaded and follow the installation instructions.


* Option 2: Install Python only **Only for advance users**
Install whatever you want.


#### PyCharm Installation
PyCharm is the best Python IDE... mmm don't know if it's the best but is a good one, if you don't like it you can choose another one

***Steps***
1.   Go to the [PyCharm download page](https://www.jetbrains.com/pycharm/download/#section=windows).
2.   Choose your operating system.
3.   **Select the Community edition**
4.   Download PyCharm.
5.   Click on the .exe file you just downloaded and follow the installation instructions.


### Where to find course material?

Well...you might have heard about Git/Gitlab (you know the octocat...) if not, don't worry we will also learn in this course :) ... only if you want though!

> For the moment you will need an extra account to access all the content which is available in the following link: [imdc-learning-python](https://gitlab.com/imdc/imdc-learning-python) but don't worry we will explain this later on.

In the meantime, here you can see the syllabus of the course:
## Unit 1: Introduction

- What is **Python**?
- Why using **Python**?
- Running Python?
    - Using **virtual environtments**
- Using **PyCharm**

## Unit 2: **Python** basics I

-   Variables and types
-   Variable assigment
-   Basic types
-   Other types

## Unit 3: **Python** basics II
-   Functions
-   Conditionals
-   Loops
-   Debugging **Python** code

## Unit 4: Coding style
- guideliness
- documentation
- best practices
- Code smell
- Version control (**Git**) - Optional

## Unit 5: IO and string formatting
- reading and writing files

## Unit 6: Data manipulation
- Using **Pandas**, **Numpy** and **Matplotlib**

## Unit 7: Object Oriented Progamming
- How to reuse my code?
- Using Classes
- Using Interfaces