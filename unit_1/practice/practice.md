## Exercise 1

Write a python program to check if the number entered is higher than 25. If not then continue asking for new input.

*   Use at least 2 different functions
*   Do not use loops in your solution.

## Excercise 2

Write a program that asks the user to enter their name and their age. Print out a message addressed to them that tells them the year that they will turn 100 years old.