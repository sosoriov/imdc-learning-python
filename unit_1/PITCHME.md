# Python

### Unit 1: Introduction

---

## What is Python?

<br>
![python logo](https://www.python.org/static/img/python-logo.png)

Python is an **interpreted**, **object-oriented**, **high-level programming** language with **dynamic semantics**.

---

## Why Python?

> did you know that **Python** is *slow*?

- Java | 
    - 3 - 5 times shorter |
- C++ |
    - 5 - 10 times shorter |
- Matlab |
    - Commercial |
    - "Whole Package", numerical computing environtment and PL including IDE |

--- 
## Why Python?

What people love about Python is how it scales so well from writing simple prototypes to validate an idea, all the way to building “production grade” systems.

Python spans the gamut from `print('hello, world')` all the way to running the back-end infrastructure for massive applications like Reddit, Instagram, or YouTube.

---
## When to use Python? 
### is Python always the right choice?

No. ***No single programming language is.***

For example, writring a real-time operating system kernel in Python.

**But**...Python to build web applications, generate reports, automate tests, conduct research, etc.

---?image=assets/image/imdc_python_use_cases.png

---
## Why Python at IMDC?

[developers survey] (https://www.jetbrains.com/research/python-developers-survey-2017/)

---
### Where / how to find the course material?

@fa[gitlab gp-contact](https://gitlab.com/sosoriov/imdc-learning-python)

---
## How to start?
#### Python Installation:

* Option 1: Install Anaconda. **RECOMMENDED**!
Anaconda is a free Python distribution that includes many commonly used Python packages.

* Option 2: Install Python only **Only for advance users**
Install whatever you want.

---
*** Anaconda Steps***
[Installation] (https://gitlab.com/sosoriov/imdc-learning-python/blob/master/unit_0/intro.md#python-installation)

*** Pycharm Steps***
[Installation] (https://gitlab.com/sosoriov/imdc-learning-python/blob/master/unit_0/intro.md#python-installation)

---
## Checking your Python installation

-   Open a windows console (press windows + r and type 'cmd') |
-   Type python |

---?image=assets/image/console_python.png

---?code=assets/code/hello_world.py&lang=python&title=Hello World

@[1,1](Python inline comments)

---?code=assets/code/hello_variables.py&lang=python&title=Variables
@[2,2](Variable definition)
@[1-3](Function definition)
@[6,6](Function execution)

---?code=assets/code/input.py&lang=python&title=Input
@[2,2](Asking user input)

--- 

## Explore Python *object*

**use `dir()` and 'help()'**

---
## Indentation
```matlab
[mConc,mDis,mFlux,mVelX,mVelY,mdist,mDepthBins,vTrange,mBott,mUTM,mDateNum] = IMDC_Read_ADCP_simple(cA,strYN);

Vmag=sqrt(mVelX.^2 + mVelY.^2);
if mConcSalCurr(1)==1
%mConc = mConc;
elseif mConcSalCurr(1)==2
beta=atan2(mVelY,mVelX)-pi()*(90-TrackAng)/180;
mConc = 0.001*mConc.*Vmag.*sin(beta); %x0.001 om naar kg/m³ te gaan, *Vmag om de flux in kg/m²/s te berekenen
elseif mConcSalCurr(1)==3
beta=atan2(mVelY,mVelX)-pi()*(90-TrackAng)/180;
mConc = 0.001*mConc.*Vmag.*cos(beta);
elseif mConcSalCurr(3)==1
beta=atan2(mVelY,mVelX)-pi()*(90-TrackAng)/180;
mConc = Vmag.*sin(beta);
elseif mConcSalCurr(4)==1
beta=atan2(mVelY,mVelX)-pi()*(90-TrackAng)/180;
mConc = Vmag.*cos(beta);
end 

```
@[1-60](How the hell can you read/write this?)
@[4-8](Did you notice there are some conditionals statement?)
@[4-8](easy to make mistakes)

---
## STOP!

![my eyes hurt](https://c1.staticflickr.com/6/5459/7096096861_37dde52968_b.jpg)


---?code=assets/code/indent_demo_1.py&lang=python&title=Indentation I
@[2,2](Indentation)
@[1-9](What is the output?)

---?code=assets/code/indent_demo_2.py&lang=python&title=Indentation II
@[1,1](New variable)
@[8,9](Conditionals - Indentation)
@[1-20](What is the output?)

---?code=assets/code/indent_demo_3.py&lang=python&title=Indentation III
@[9,10](Conditionals - Indentation)
@[1-30](What is the output?)

---

## PyCharm

![python salvation](http://www.doinghistime.org/wp-content/uploads/2016/02/salvation.png)

---
## Let's do some coding

Write the following program, try to use at least 2 different functions

---?image=assets/image/program_1.png&size=auto 90%

---
## Resources

[Python official website] (https://www.python.org/)
[Pep8] (https://www.python.org/dev/peps/pep-0008/)
[Anaconda] (https://www.anaconda.com/download/)

---

### Questions?

