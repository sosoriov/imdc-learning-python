# function definition: use keyword def
def sayHelloWorld():
    greeting = "Hello, world!"
    print(greeting)


# function execution, call the fuction using ()
sayHelloWorld()

# function accepts parameters


def sayHelloParameters(name):
    greeting = "Hello"
    print(greeting, name)


# pass function parameteres in ()
sayHelloParameters("IMDC")
