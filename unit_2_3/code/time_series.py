water_levels = []

def get_last_wl_value():
    return water_levels[-1]

def add_wl_value(wl_value, last_value=[0]):
    if wl_value > 10:
        return None

    water_levels.append([last_value, wl_value])

def get_user_option():
    return float(input("Please enter your Option:"))

def print_wl_values():
    for water_level in water_levels:
        print("This is the wl value")
        print(water_level)

add_wl_value(1.5)

while True:
    user_option = get_user_option()
    if user_option == 1: # enter new value
        wl_input = float(input("Please enter a WL value:"))
        add_wl_value(wl_input, get_last_wl_value())
    elif user_option == 2:
        print_wl_values()
    elif user_option == 3:
        break
    else:
        print("Option is not valid")

    # output line
    print('_' * 20)