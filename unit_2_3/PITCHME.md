# Python

### Unit 2: Diving Into the Basics of Python

---

## Recap I

#### How to run python?

- Option 1: console |
- Option 2: executing files |

---
## Recap I - Running a python script

```python
python nameofmyfile.py
```

*python file extension **.py** *

---

## Recap II

#### Python playground

[Python official website] (https://www.python.org/)

---

##  Python Data Types

- Numbers |
    - Integers (10, -4) |
    - Floats (1.5, -33.4) |
- Booleans |
    - True |
    - False |
- Strings |
    - 'Hi there' |
    - "Hi there" |
- Complex Types like Dictionaries & Objects - covered later |

---

## Python Data Types: Numbers

- Integers (10, 4) |
    - As big (small) as supported by your Memory and Operation System |
- Floats (43.4, -1.4) |
    - As big (small) as supported by your Memory and Operation System |

Note:
Remember to show an example in the console with age as string and applying operator +.
```
age = "22"
age + 1
```
show the `int()` function
show the `float()` function

---
## Python Data Types: Operators

[Operators](https://www.screencast.com/t/5NTQu1e1U)

Note:
Show example with + in strings and *  with lines 

---

## When Working with Numbers

#### 1 - 0.9 = ?

[Floating point] (https://docs.python.org/3/tutorial/floatingpoint.html)

---
## When Working with Strings

- "Hello World" |
- 'Hello World' |

Note:
Show advantaces using "" because you can write something like this:
```
greeting = "Hello, I'm Sebastian!"
```
Show also the multiline support with """ character

---
## Lists

#### Data structure to store *elements*. Each element or value that is inside of a list is called an item.

[Python list](https://www.screencast.com/t/GwwOurk6k)

---
## Lists Operations

- Adding elements: **append()**
- Removing elements **pop()**

[List Documentation] (https://docs.python.org/3.6/tutorial/datastructures.html)

---
## Problem

Assume there is a ship which is measuring water levels in an area.
We don't know any information about the ship and the data is being measured every X minutes.
We would like to collect the water level information but due to some connectivity problems in the area, sometimes the data is corrupted.
The mission is to write a program which can store, display and validate the water level data.


![ship](http://hostagencyreviews.com/wp-content/uploads/2012/06/cruise-ship-tracker-path.jpg)

---

## Proposed Solution

[Solution](https://www.screencast.com/t/K2qXOxp9SkZk)

---
## Functions

### return

A function can return values

```python
def sum(a, b):
    return a + b

print(sum(5, 3))
```

Note:
refactor function to use return for last value

---
### Default arguments

```python
def greet(name, age=29):
    print('Hello ' + name + ', I am ' + age)


print(greet('Sebastian'))
```

Note:
refact function to pass [1] as a default parameter

---
### Keyword arguments (kwargs)

```python
def greet(name, age):
    print('Hello ' + name + ', I am ' + age)


print(greet(age=31, name='Sebastian'))
```

Note:
show how to call it with named params
Use input function to ask user input... duplicate the code 
```python
wl_value = float(input("Enter the new wl value: "))
```
refactor the code and extract the input to a function


---
## Loops

[Python loops](https://www.screencast.com/t/Ty0qkLgbMbh)

Note:
change last print and add for lop for printing. add while loop to enter inf options

---
## Conditionals -- Booleans

[Booleans](https://www.screencast.com/t/EPWwBkPAPxX)

Note:
First show operators in console.
```python
1 == 1
```
show also IN operator
adding grouping operators:
```python
name = 'Seb'
age = 31
if name == 'Chris' and age > 20 or age < 30:
    print("Yes!")
```
Create option 1 to enter value
Create option 2 to print
Create else option for invalid
Create Q for quit

---
## Resources

[Python official website] (https://www.python.org/)
[Pep8] (https://www.python.org/dev/peps/pep-0008/)
[Anaconda] (https://www.anaconda.com/download/)

---

### Questions?

