# IMDC LEARNING PYTHON

## Unit 1: Introduction

#### [Presentation - Click here!](https://gitpitch.com/sosoriov/imdc-learning-python/master?grs=gitlab&p=unit_1)

- What is **Python**?
- Why using **Python**?
- Running Python?
- Variables
- Using **PyCharm**

## Unit 2: **Python** Diving Into the Basics of Python

#### [Presentation - Click here!](https://gitpitch.com/sosoriov/imdc-learning-python/master?grs=gitlab&p=unit_2_3)

-   Variables and types
-   Variable assignment
-   Basic types
-   Other types

## Unit 3: **Python** Working with Loops & Conditionals

#### [Presentation - Click here!](https://gitpitch.com/sosoriov/imdc-learning-python/master?grs=gitlab&p=unit_2_3)

-   Functions
-   Conditionals
-   Loops

## Unit 4: IO and string formatting

#### [Presentation - Click here!](https://gitpitch.com/sosoriov/imdc-learning-python/master?grs=gitlab&p=unit_4#)

- reading and writing files

## Unit 5: Coding style
- Using **virtual environtments**
- guideliness
- documentation
- best practices
-   Debugging **Python** code
- Code smell
- Version control (**Git**) - Optional


## Unit 6: Data manipulation
- Using **Pandas**, **Numpy** and **Matplotlib**

## Unit 7: Object Oriented Progamming
- How to reuse my code?
- Using Classes
- Using Interfaces