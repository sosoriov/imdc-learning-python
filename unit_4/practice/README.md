# Practice

Read [wdt_results.csv](wdt_results.csv) file and answer the following questions:

* What is the **Wind Speed** average?
* Plot the SWH (Significant Wave Height) for the vessel *Jack-Up type 3*

    **Hint**: use the **plot_list** function available in [practice.py](practice.py)
    **Hint**: Cast your data to the right data type, use int() or float() functions
    **Hint**: Use split() function to split strings



