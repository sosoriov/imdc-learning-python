import matplotlib.pyplot as plt

def plot_list(values):
    """Plot all values in the given list. You should pass only numbers
    Keyword arguments:
    values --> example: [1, 8, 6]"""
    plt.plot(values)
    plt.ylabel('Add your label here!')
    plt.show()


# plot_list([1, 8, 6])
