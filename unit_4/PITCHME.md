# Python

### Unit 4: Reading and Writing Files

---

## Handling File Access Correctly

#### Steps:
1. open()
2. Do operations -> write() or readline()
3. close()

---

## Writing files

```python
f = open('demo.txt', mode='w')
f.write('Hello World!')
f.close()
```
---

## Different open() modes

- **r**  -> Read access only
- **w**  -> Write Access Only
- **r+** -> Read & Write Access
- **x**  -> Write Access Only: Exclusive Creation, Fails if File Exists
- **a**  -> Write Access Only: Append to End of File if it Exists
- **b**  -> Open in Binary Mode (for Writing Binary Data)

--- 

## Reading files I

```python
f = open('demo.txt', mode='r')
file_content = f.read()
print(file_content)
f.close()
```

--- 

## Reading files II

Useful functions.

* Read all lines in the file 
`readlines()` 

* Read the file line by line
`readline()`

#### Recomendation!!

Use **with**

---
## Resources

[Python official website] (https://www.python.org/)
[Pep8] (https://www.python.org/dev/peps/pep-0008/)
[Anaconda] (https://www.anaconda.com/download/)

---

### Questions?

